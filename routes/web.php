<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OmeniController;
use Illuminate\Support\Facades\Route;


Route::get('/', [BlogController::class, 'index']);
//Route::get('/omeni/', [BlogController::class, 'omeni']);
Route::get('/detalji/{id}', [BlogController::class, 'findOne']);
Route::delete('/{id}', [BlogController::class, 'delete'])->middleware('auth');
Route::post('/', [BlogController::class, 'save'])->middleware('auth');
Route::get('/update/{id}', [BlogController::class, 'update'])->middleware('auth');
Route::put('/{id}', [BlogController::class, 'edit'])->middleware('auth');
Route::get('/create', [BlogController::class, 'create'])->middleware('auth');
Route::post('/', [BlogController::class, 'store'])->middleware('auth');

Route::get('/registration', [UserController::class, 'registration']);
Route::post('/user_registration', [UserController::class, 'registerUser']);
Route::post('/logout', [UserController::class, 'logout'])->middleware('auth');
Route::get('/login', [UserController::class, 'login']);
Route::post('/authentication', [UserController::class, 'authentication']);

Route::get('/uredi/{id}', [OmeniController::class, 'uredi'])->middleware('auth');
Route::put('/{id}', [OmeniController::class, 'edit'])->middleware('auth');
Route::get('/omeni', [OmeniController::class, 'omeni']);
//Route::get('/omeni/{id}', [OmeniController::class, 'omeni']);