@extends('layout')

@section('content')


<form method="POST" action="/{{$omeni->id}}">
    @csrf
    @method("PUT")
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Ime</label>
      <input type="text" name="firstName" value="{{$omeni->firstName}}" class="form-control" id="exampleInputName" aria-describedby="nameHelp">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Prezime</label>
        <input type="text" name="lastName" value="{{$omeni->lastName}}" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Datum rođenja</label>
      <input type="text" name="dob" value="{{$omeni->dob}}" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Škola</label>
      <input type="text" name="school" value="{{$omeni->school}}" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Faks</label>
      <input type="text" name="university" value="{{$omeni->university}}" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Posao</label>
      <input type="text" name="work" value="{{$omeni->work}}" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Hobi</label>
      <input type="text" name="hobby" value="{{$omeni->hobby}}" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Specijalni talent</label>
      <input type="text" name="specTalent" value="{{$omeni->specTalent}}" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
    </div>
    <button type="submit" class="btn btn-primary">Spremi</button>
</form>

@endsection