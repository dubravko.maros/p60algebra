@extends('layout')
@section('content')

<div class="row">
  <div class="col-sm-10">
    <form action="/">
      <div class="input-group mb-3">
        <input type="text" name="search" class="form-control" placeholder="Pretraživanje po tekstu i datumu" aria-label="Pretraživanje teksta" aria-describedby="button-addon2">
        <button class="btn btn-outline-primary" type="submit" id="button-addon2">Traži</button>
      </div>
    </form>
  </div>
  @auth
  <div class="col-sm-2">
    <a type="button" class="btn btn-primary" href="/create">Unos novog bloga</a>
  </div>
  @endauth
      <div class="container"> 
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Datum objave</th>
              <th scope="col">Tekst</th>
              @auth
              <th scope="col">Akcije</th>
              @endauth
            </tr>
          </thead>
          <tbody>
            @foreach ($blogs as $key=>$blog)
            <tr>
              <th scope="row">{{{$key + 1}}}</th>
              <td>{{$blog->datum}}</td>
              <td><a href="/detalji/{{$blog->id}}">{{$blog->blogTekst}}</a></td>
              @auth
              <td>
                <div class="row">
                  <div class="col-sm-5">
                    <a class="btn btn-outline-warning" href="/update/{{$blog->id}}">Uredi</a>
                  </div>
                  <div class="col-sm-5">
                    <form method="POST" action="/{{$blog->id}}">
                      @csrf
                      @method("DELETE")
                      <button class="btn btn-outline-danger">Izbriši</button>
                    </form>
                  </div>
                </div>
                @endauth
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
</div>

@endsection








