@extends('layout')

@section('content')

    <div class="container"> 
      @foreach ($omenis as $key=>$omeni)
      @auth
      <div class="row">
        <div class="col-sm-5">
          <a class="btn btn-outline-warning" href="/uredi/{{$omeni->id}}">Uredi</a>
        </div>
      </div>
      @endauth
    <table class="table table-borderless">
        <tbody>
          <tr>
            <th scope="row"><p class="text-end">Ime</p></th>
            <td>
                
                    <p class="fw-bold">{{$omeni['firstName']}}</p>
            </td>
          </tr>
          <tr>
            <th scope="row"><p class="text-end">Prezime</p></th>
            <td>{{$omeni['lastName']}}</td>
          </tr>
          <tr>
            <th scope="row"><p class="text-end">Datum rođenja</p></th>
            <td>{{$omeni['dob']}}</td>
          </tr>
          <tr>
            <th scope="row"><p class="text-end">Škola</p></th>
            <td>{{$omeni['school']}}</td>
          </tr>
          <tr>
            <th scope="row"><p class="text-end">Faks</p></th>
            <td>{{$omeni['university']}}</td>
          </tr>
          <tr>
            <th scope="row"><p class="text-end">Posao</p></th>
            <td>{{$omeni['work']}}</td>
          </tr>
          <tr>
            <th scope="row"><p class="text-end">Hobi</p></th>
            <td>{{$omeni['hobby']}}</td>
          </tr>
          <tr>
            <th scope="row"><p class="text-end">Specijalni talent</p></th>
            <td>{{$omeni['specTalent']}}</td>
            @endforeach
          </tr>
        </tbody>
      </table>

    </div>
    </div>
 
@endsection