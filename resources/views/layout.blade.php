<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    
    <title>{{$title}}</title>
  </head>
  <body>
    <nav class="navbar navbar-dark bg-primary">
      <div class="container-fluid">
        <div class="col align-self-center">
          <a class="navbar-brand" href="/">Blog</a>
          <a class="navbar-brand" href="/omeni">O meni</a>
        </div>
        <div class="col align-self-center">
          <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            @auth
              <form method="POST" action="/logout">
                @csrf
                <button class="btn btn-outline-light" type="submit">Odjavi se</a>
              </form>
            @else
              <a class="navbar-brand" aria-current="page" href="/registration">Registriraj se</a>
              <a class="btn btn-outline-light" href="/login">Login</a>
            @endauth
          </div>
        </div>
      </div>
    </nav>
    <br>
    <div class="container"> 
    <h1>{{$title}}</h1>
    <br>
    @yield('content')
    </div>

  </body>
</html>
