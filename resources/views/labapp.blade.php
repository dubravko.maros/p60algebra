<!doctype html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/icoKBM.ico" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Laboratorij distribucija</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #333a3e;">
    <!--<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #e30613;"> crvena traka -->
     

      <div class="container-fluid">
        <div class="col align-self-center">
        <div class="col align-self-center">
        <img src="img/KBM_logo_bijeli.svg" alt="" width="150" height="40" class="img-fluid">
        </div>
       
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
        </div>

        </div>
        <div class="col align-self-center">
        <h2 class="text-center" style="color:white">Laboratorij distribucija</h2>
        </div>
        <div class="col align-self-center">
          <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <button class="btn btn-outline-light" type="submit">Izlaz</button>
          </div>
        </div>
      </div>


    </nav>

   
    <br><br>
    <div class="container">
      <!-- Button trigger modal -->
      <div class="row">
      <div class="col align-self-start">
        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Preuzimanje novog uzorka</button>
        <!-- <button type="button" class="btn btn-secondary" style="background-color: #333a3e;" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Preuzimanje novog uzorka</button> -->
      </div>
      <div class="col align-self-end">
     
      <div class="d-flex justify-content-end">
        <button type="button" class="btn btn-danger">Nadzor</button>
        <!-- <button type="button" class="btn btn-danger" style="background-color: #e30613;">Nadzor</button> -->
      </div>
      </div>
      </div>
      <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Novi uzorak</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <div class="form-floating mb-3">
                  <input type="text" class="form-control" id="korisnik" placeholder="Korisnik">
                  <label for="korisnik">Skeniranje kartice djelatnika</label><br>
                  <h7 class="fst-italic" style="color:black">Očitan djelatnik:</h7><br><br>        
                </div>
                <h7 class="fst-italic" style="color:black">Materijal zaprimljen s odjela:</h7><br><br>    
                <div class="col-12">
                  <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                  <select class="form-select" id="inlineFormSelectPref">
                    <option selected>Odjel</option>
                    <option value="1">Odjel za hematologiju</option>
                    <option value="2">Odjel za kardiologiju</option>
                    <option value="3">Odjel za gastroenterologiju</option>
                  </select>
                </div>
                <br><br>
                <div class="form-floating mb-3">
                  <input type="text" class="form-control" id="oib" placeholder="OIB">
                  <label for="oib">Skeniranje kartice laboratorijskog djelatnika</label><br>
                  <h7 class="fst-italic" style="color:black">Očitan laboratorijski djelatnik:</h7><br><br>
                </div>
                <br><br>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Prekid</button>
                <button type="button" class="btn btn-secondary">Spremi unos</button>
              </div>
            </div>
          </div>
        </div>
      <!-- kraj Modal -->
      <br><br>
      <br><br>
      <h7 class="fst-italic" style="color:black">Ispis preuzetih uzoraka na današnji dan</h7><br><br>
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Ime</th>
            <th scope="col">Prezime</th>
            <th scope="col">Odjel</th>
          <th scope="col">Preuzeo</th>
          <th scope="col">Datum i vrijeme zaprimanja uzorka</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">3</th>
            <td>Ana</td>
            <td>Anić</td>
            <td>Odjel za hematologiju</td>
            <td>Zoran Šiftar</td>
            <td>07.03.2024 10:32:08</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Pero</td>
            <td>Perić</td>
            <td>Odjel za kardiologiju</td>
            <td>Mirjana Mariana Kardum Paro</td>
            <td>07.03.2024 09:06:51</td>
          </tr>
          <tr>
            <th scope="row">1</th>
            <td>Marko</td>
            <td>Markić</td>
            <td>Odjel za hematologiju</td>
            <td>Mirjana Mariana Kardum Paro</td>
            <td>07.03.2024 08:45:16</td>
          </tr>
        </tbody>
      </table>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>