@extends('layout')
@section('content')
<div class="row">
    <form method="POST" action="/">
        @csrf
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Datum</label>
          <input type="date" name="datum" class="form-control" id="exampleInputName" aria-describedby="nameHelp">
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Tekst</label>
            <input type="text" name="blogTekst" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
        </div>
        <button type="submit" class="btn btn-primary">Spremi</button>
    </form>
</div>
@endsection
