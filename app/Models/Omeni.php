<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Omeni extends Model
{
    use HasFactory;

    protected $fillable = ['datum', 'blogTekst', 'dob', 'school', 'university', 'work', 'hobby', 'specTalent'];
   
}
