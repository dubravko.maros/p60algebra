<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Blog extends Model
{
    use HasFactory;

    protected $fillable = ['datum', 'blogTekst'];

    public static function getAll($request)
    {
        $blogs = DB::table('blogs');
if ($request->get('search') != null && $request->get('search') != '')
{
    $blogs->where('blogTekst', 'LIKE', '%'.$request->get('search').'%');
    $blogs->orWhere('datum', 'LIKE', '%'.$request->get('search').'%');
}
        return $blogs->get();
    }
    
}
