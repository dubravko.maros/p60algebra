<?php

namespace App\Http\Controllers;


use App\Models\Blog;
use Illuminate\Http\Request;


class BlogController extends Controller
{
    public function index(Request $request)
    {
        
        return view('blog',
    [
        'title' => 'Blog',
        'blogs' => Blog::getAll($request)
    ]);
    } 

   
    
    public function findOne($id)
    {
        $stranicaDetalji = Blog::find($id);

        if (isset($stranicaDetalji) && $stranicaDetalji != '')
        {
        return view('detalji',
    [
        'title' => 'Detalji',
        'blogs' => $stranicaDetalji
    ]);
    }
    abort(404);
    } 
    public function delete($id)
    {
        $blog = Blog::find($id);
        $blog->delete();

        return redirect('/');
    }
    public function save(Request $request)
    {
       
        $formData = $request->validate([
        'datum' => 'required',
        'blogTekst' => 'required'
]);


        Blog::create($formData);
        return redirect('/');
    }
    public function update($id)
    {
        $blog = Blog::find($id);
        return view('update',
    [
        'title' => 'Uređenje bloga',
        'blog' => $blog
    ]);
    } 
    public function edit(Request $request, $id)
    {
        $blog = Blog::find($id);

        $blog['datum'] = $request->datum;
        $blog['blogTekst'] = $request->blogTekst;
        $blog->save();

        return redirect('/');
    }

    // public function create()
    // {
    //     $blog = Blog::find($id);

    //     $blog['datum'] = $request->datum;
    //     $blog['blogTekst'] = $request->blogTekst;
    //     $blog->save();

    //     return redirect('/');
    // }
    public function create()
    {
        return view('create',
        [
        'title' => 'Unos bloga'
    ]);
}

    public function store(Request $request)
    {
        $formFields = $request->validate([
            'datum' => 'required',
            'blogTekst' => 'required'
        ]);

        Blog::create($formFields);

        return redirect('/');
    }
}
