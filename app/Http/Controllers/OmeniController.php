<?php

namespace App\Http\Controllers;
use App\Models\Omeni;
use Illuminate\Http\Request;

class OmeniController extends Controller
{
    public function uredi($id)
    {
        $omeni = Omeni::find($id);
        return view('uredi',
    [
        'title' => 'Uređenje',
        'omeni' => $omeni
    ]);
    } 
    public function edit(Request $request, $id)
    {
        $omeni = Omeni::find($id);

        $omeni['firstName'] = $request->firstName;
        $omeni['lastName'] = $request->lastName;
        $omeni['dob'] = $request->dob;
        $omeni['school'] = $request->school;
        $omeni['university'] = $request->university;
        $omeni['work'] = $request->work;
        $omeni['hobby'] = $request->hobby;
        $omeni['specTalent'] = $request->specTalent;
        $omeni->save();

        return redirect('/omeni');
    }
    public function omeni()
    {
        $stranicaOmeni = Omeni::all();
        return view('omeni',
    [
        'title' => 'O meni',
        'omenis' => $stranicaOmeni

    ]);
    } 
}
